How to clone?

    $ git clone ...
    $ git submodule update --init

How to remove docker images and volumes (good after docker-compose kill)?

    ./cleanup-docker.sh
